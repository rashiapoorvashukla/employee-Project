import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { catchError, map, Observable, tap, throwError } from 'rxjs';
import { CreateEmployeeComponent } from './create-employee/create-employee.component';
import { Department } from './department';
import { Employee } from './employee';
import { Project } from './project';
import { State } from './state';
import { City } from './city';


@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  group(arg0: { state: (string | ((control: import("@angular/forms").AbstractControl<any, any>) => import("@angular/forms").ValidationErrors | null))[]; city: string[]; }): Employee {
    throw new Error('Method not implemented.');
  }

  private baseURL = "http://localhost:8080/api/v1/employees";

  constructor(private httpClient: HttpClient) { }
  
  getEmployeesList(): Observable<Employee[]>{
    return this.httpClient.get<Employee[]>(this.baseURL);
  }

getDepartments() : Observable<Department>  {
  return this.httpClient.get<Department>('http://localhost:8080/api/v1/departments')

}
getProjects() {
  return this.httpClient.get<Project>('http://localhost:8080/api/v1/projects');
}
getStates(){
  return this.httpClient.get<State>('http://localhost:8080/api/v1/state');
}
getCities(stateId: number):Observable<City[]>{
  let myNewLinkToFetchCities = 'http://localhost:8080/api/v1/city/'+stateId;
  return this.httpClient.get<City[]>(myNewLinkToFetchCities);
}
createEmployees(employee: Employee): Observable<Object>{
  return this.httpClient.post((this.baseURL),employee);
}
// deleteEmployee(id: number): Observable<Object>{
//   return this.httpClient.delete(`${this.baseURL}/${id}`);
// }
}