import { City } from "./city";
import { Department } from "./department";
import { Project } from "./project";
import { State } from "./state";

export class Employee {
    id: number;
    firstName: string;
    lastName: string;
    salary: number;
    designation: string;
    state: State;
    city: City;
    project:Project;
    department: Department; 


}
