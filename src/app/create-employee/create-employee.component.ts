import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { City } from '../city';
import { Department } from '../department';
import { Employee } from '../employee';
import { EmployeeService } from '../employee.service';
import { Project } from '../project';
import { State } from '../state';
 //import { Department } from '../department';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {
//   

  employee: Employee = new Employee();

 //dropdownOptions: Department[];

  department: Department;
  departmentOption:any;

  project:Project;
  projectOption: any;

  state:State;
  stateOption: any;

  city:City;
  cityOption: any;

  constructor (private employeeService: EmployeeService ,
  private router: Router) {
    this.employee.department=new Department();
    this.employee.project=new Project();
    this.employee.state=new State();
    this.employee.city=new City();
  }

  ngOnInit(){
   // debugger;
  this.employeeService.getDepartments().subscribe(
    data=> { 
     this.departmentOption=data;
    },
  );

  this.employeeService.getProjects().subscribe(
    (data) => {
      this.projectOption=data;
    },
  );


  this.employeeService.getStates().subscribe(
    (data) => {
      this.stateOption=data;
    },
  );

  // this.employeeService.getCities().subscribe(
  //   (data) => {
  //     this.cityOption=data;
  //   },
  // );
  }

  saveEmployee(){
    debugger;
    this.employee.state = this.stateOption.filter(
      (stateObject:any)=>{
        debugger;
        return stateObject.id == this.employee.state;
      }
    )?.[0]?.stateName;
    this.employeeService.createEmployees(this.employee).subscribe(data =>{ 
      this.employee = new Employee();
      this.goToEmployeeList();
    },)
  }

  goToEmployeeList(){
    this.router.navigate(['/employees']);
  }
  
  onSubmit(): void {
    console.log(this.employee)
    this.saveEmployee();
  }


  
  handleStateChange(event:any){
    console.log(event);
    let stateId: number = event;
    this.employeeService.getCities(stateId)
    .subscribe(
    (data) => {
      this.cityOption=data;
    },
  );
  }
}



